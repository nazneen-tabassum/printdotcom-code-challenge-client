import Router from "vue-router";
import Folder from "./components/Folders.vue";
import Posters from "./components/Posters.vue";
import BusinessCards from "./components/BusinessCards.vue";

const routes = [
  {
    path: "/Product/Posters",
    name: "Posters",
    component: Posters,
  },
  {
    path: "/Product/BusinessCards",
    name: "BusinessCards",
    component: BusinessCards,
  },
  {
    path: "/Product/Folder",
    name: "Folders",
    component: Folder,
  },
];

export default new Router({ mode: "history", routes });
