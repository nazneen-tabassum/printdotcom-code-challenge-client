This repository is craeted for the front-end challenge with below requirements.

### Requirements

Create a product selector that works for three different products from our catalogue.

1. Navigate via a menu to a product:
   /product/posters
   /product/folders
   /product/businesscards
2. See a product selector with available options (see json files in this Repo).
3. Be able to select different options in the product
4. Add a product to a cart
5. Validate if the options is a possible option via the 'excludes' property.

### Implementation

At this moment, Vue is new framework to me and I am still in learning process. I have done coding in parallel with my learning for this challenge and the challenge has not finished yet.

For now, I have made some progress on this using Vue framework with following libraries installed.

- vue
- @vue/cli
- vue-router
- bootstrap-vue
